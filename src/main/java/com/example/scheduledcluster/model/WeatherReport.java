package com.example.scheduledcluster.model;


import java.util.List;

public class WeatherReport {
    private Main main;
    private List<Weather> weather;
    private Integer visibility;

    @Override
    public String toString() {
        return "Current weather status: " + weather.get(0).getMain() + "\n" +
                "- Temperature: " + (main.getTemp() - 273.15) + "°C\n" +
                "- Humidity: " + main.getHumidity() + "%" + "\n" +
                "- Visibility: " + (visibility / 1000) + "km";
    }


    public class Main {
        private Double temp;
        private Integer humidity;

        public Double getTemp() {
            return temp;
        }

        public Integer getHumidity() {
            return humidity;
        }

        @Override
        public String toString() {
            return "Main{" +
                    "temp=" + temp +
                    ", humidity=" + humidity +
                    '}';
        }
    }

    public class Weather {
        private String main;

        public String getMain() {
            return main;
        }

        @Override
        public String toString() {
            return "Weather{" +
                    "main='" + main + '\'' +
                    '}';
        }
    }
}
