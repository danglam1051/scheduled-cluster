package com.example.scheduledcluster.tasks;

import com.example.scheduledcluster.service.EmailSender;
import net.javacrumbs.shedlock.core.SchedulerLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.mail.internet.AddressException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class WeatherTask {
    private Logger log = LoggerFactory.getLogger(WeatherTask.class);

    private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    private EmailSender emailSender;

    @Scheduled(cron = "0 0 8 * * *")
    @SchedulerLock(name = "scheduledTaskName", lockAtLeastFor = 82800000, lockAtMostFor = 82800000) //Lock for 23 hours
    public void test() {
        log.info("at {}", dateFormat.format(new Date()));

        try {
            emailSender.sendEmail();
        } catch (AddressException e) {
            log.info(e.getMessage());
        }
    }
}
