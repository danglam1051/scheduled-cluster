package com.example.scheduledcluster.service;

import com.example.scheduledcluster.config.Constants;
import it.ozimov.springboot.mail.model.Email;
import it.ozimov.springboot.mail.model.defaultimpl.DefaultEmail;
import it.ozimov.springboot.mail.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class EmailSender {

    @Autowired
    private EmailService emailService;

    @Autowired
    private WeatherService weatherService;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public void sendEmail() throws AddressException {
        List<InternetAddress> toEmailList = new ArrayList<>();
        String emailSubject = "Weather report of Hanoi at " + dateFormat.format(new Date());

        for (String email : Constants.TO_EMAIL_LIST) {
            toEmailList.add(new InternetAddress(email));
        }

        final Email email = DefaultEmail.builder()
                .from(new InternetAddress(Constants.FROM_EMAIL))
                .to(toEmailList)
                .subject(emailSubject)
                .body(weatherService.retrieveCurrentWeatherReport().toString())
                .encoding(String.valueOf(Charset.forName("UTF-8"))).build();

        emailService.send(email);
    }
}
