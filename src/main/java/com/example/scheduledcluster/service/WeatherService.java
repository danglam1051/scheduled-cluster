package com.example.scheduledcluster.service;

import com.example.scheduledcluster.config.Constants;
import com.example.scheduledcluster.model.WeatherReport;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class WeatherService {
    public WeatherReport retrieveCurrentWeatherReport() {
        RestTemplate restTemplate = new RestTemplate();
        WeatherReport weatherReport;
        Gson gson = new Gson();

        String response = restTemplate.getForObject(Constants.CURRENT_WEATHER_API_URL, String.class);
        weatherReport = gson.fromJson(response, WeatherReport.class);

        return weatherReport;
    }
}
