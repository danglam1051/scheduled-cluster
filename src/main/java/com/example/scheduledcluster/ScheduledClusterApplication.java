package com.example.scheduledcluster;

import it.ozimov.springboot.mail.configuration.EnableEmailTools;
import net.javacrumbs.shedlock.spring.annotation.EnableSchedulerLock;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableSchedulerLock(defaultLockAtMostFor = "PT30S")
@EnableEmailTools
public class ScheduledClusterApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScheduledClusterApplication.class, args);
    }
}

