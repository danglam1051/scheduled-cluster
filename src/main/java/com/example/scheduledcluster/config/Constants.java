package com.example.scheduledcluster.config;

public final class Constants {
    public static final String FROM_EMAIL = "lampdse04797@fpt.edu.vn";
    public static final String[] TO_EMAIL_LIST = {"tony.thian@oncloud.tech"};
    public static final String CITY_ID = "1581129"; // Hanoi
    public static final String WEATHER_API_KEY = "23f781122b691fdf79945f99a6e4be5e";
    public static final String CURRENT_WEATHER_API_URL = "http://api.openweathermap.org/data/2.5/weather?id=" + CITY_ID + "&appid=" + WEATHER_API_KEY;
}
